package loxe

func NewLoxeValidator() *LoxeValidator {
	return &LoxeValidator{
		nil,
		map[string]error{},
		nil,
	}
}

type LoxeValidator struct {
	rootError error
	fields    map[string]error
	childs    map[string]*LoxeValidator
}
