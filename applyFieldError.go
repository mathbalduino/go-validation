package loxe

func (v *LoxeValidator) ApplyFieldError(fieldName string, e error) {
	if e == nil {
		return
	}

	v.fields[fieldName] = e
}
