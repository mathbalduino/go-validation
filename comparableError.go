package loxe

type ComparableError interface {
	Is(error) bool
}
