package custom

import (
	"bytes"
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/util"
	"gitlab.com/loxe-tools/go-validation/internal/generate/input"
	"os"
	"text/template"
)

var cliUsage = func() func() {
	tmpl, e := template.New("").Parse(cliUsageHelpTemplate)
	if e != nil {
		panic(e)
	}

	type tmplVars struct {
		SourcePackage        string
		SourcePackageDefault string
		SourceType           string
		SourceTypeDefault    string

		OutputFileName           string
		OutputFileNameDefault    string
		OutputFolderPath         string
		OutputFolderPathDefault  string
		OutputPackage            string
		OutputPackageDefault     string
		OutputPackageName        string
		OutputPackageNameDefault string
		Client                   string
		ClientDefault            string

		Debug                     string
		DebugDefault              string
		SupportsAnsi              string
		SupportsAnsiDefault       string
		OnlyAsciiTypeNames        string
		OnlyAsciiTypeNamesDefault string
		Workdir                   string
		WorkdirDefault            string
	}
	vars := tmplVars{
		SourcePackage:        util.CyanString("--" + input.MetaᐸArgumentsᐳ.SourcePackage),
		SourcePackageDefault: util.YellowString("Default: " + input.DefaultArguments.SourcePackage),
		SourceType:           util.CyanString("--" + input.MetaᐸArgumentsᐳ.SourceType),
		SourceTypeDefault:    util.YellowString("Default: " + input.DefaultArguments.SourceType),

		OutputFileName:           util.CyanString("--" + input.MetaᐸArgumentsᐳ.OutputFileName),
		OutputFileNameDefault:    util.YellowString("Default: " + input.DefaultArguments.OutputFileName),
		OutputFolderPath:         util.CyanString("--" + input.MetaᐸArgumentsᐳ.OutputFolderPath),
		OutputFolderPathDefault:  util.YellowString("Default: " + input.DefaultArguments.OutputFolderPath),
		OutputPackage:            util.CyanString("--" + input.MetaᐸArgumentsᐳ.OutputPackage),
		OutputPackageDefault:     util.YellowString("Default: " + input.DefaultArguments.OutputPackage),
		OutputPackageName:        util.CyanString("--" + input.MetaᐸArgumentsᐳ.OutputPackageName),
		OutputPackageNameDefault: util.YellowString("Default: " + input.DefaultArguments.OutputPackageName),
		Client:                   util.CyanString("--" + input.MetaᐸArgumentsᐳ.Client),
		ClientDefault:            util.YellowString(fmt.Sprintf("Default: %t", input.DefaultArguments.Client)),

		Debug:                     util.CyanString("--" + input.MetaᐸArgumentsᐳ.Debug),
		DebugDefault:              util.YellowString(fmt.Sprintf("Default: %t", input.DefaultArguments.Debug)),
		SupportsAnsi:              util.CyanString("--" + input.MetaᐸArgumentsᐳ.SupportsANSI),
		SupportsAnsiDefault:       util.YellowString(fmt.Sprintf("Default: %t", input.DefaultArguments.SupportsANSI)),
		OnlyAsciiTypeNames:        util.CyanString("--" + input.MetaᐸArgumentsᐳ.OnlyAsciiTypeNames),
		OnlyAsciiTypeNamesDefault: util.YellowString(fmt.Sprintf("Default: %t", input.DefaultArguments.OnlyAsciiTypeNames)),
		Workdir:                   util.CyanString("--" + input.MetaᐸArgumentsᐳ.Workdir),
		WorkdirDefault:            util.YellowString("Default: " + input.DefaultArguments.Workdir),
	}

	var msg bytes.Buffer
	e = tmpl.Execute(&msg, vars)
	if e != nil {
		panic(e)
	}

	return func() {
		fmt.Println(msg.String())
		os.Exit(0)
	}
}()

const cliUsageHelpTemplate = `This CLI is meant to be used with the Löxe Validation library, to generate server-side/client-side code.
	server-side: Generate typed validation schemas, based on the given GO code
	client-side: Generate helpers/glue-code to your clients, based on the server-side GO code

	Source-related flags:
		WARNING: The provided GO type(s) CANNOT contain selector ambiguity

		{{.SourcePackage}}: The import path to the package that contains the base GO source files
			This flag is always required
			Note that the "{{.Workdir}}" flag value will be used to resolve this import path
			{{.SourcePackageDefault}}
		{{.SourceType}}: The identifier of a single GO type (a struct type)
			This flag is optional
			If set, the generator will ignore all types whose name doesn't match this flag value
			This type MUST be exported if the flags "{{.OutputPackage}}" and "{{.SourcePackage}}" have different values
			{{.SourceTypeDefault}}

	Output-related flags:
		{{.OutputFileName}}: This will be the name of the generated file
			Note that this name will receive a suffix (file extension and more)
			{{.OutputFileNameDefault}}
		{{.OutputFolderPath}}: The path to the folder that the generated files will belong
			If it's a relative path, the "{{.Workdir}}" flag value will be used to resolve it
			This path will always be converted to an absolute path
			If not set, the folder of the "{{.SourcePackage}}" flag will be used instead
			{{.OutputFolderPathDefault}}
		{{.OutputPackage}}: The package import path to which the generated files will belong
			Note that the "{{.Workdir}}" flag value will be used to resolve this import path
			If not set, it will be equal to the "{{.SourcePackage}}" flag value
			If the "{{.Client}}" flag is set to true, this flag cannot be set
			{{.OutputPackageDefault}}
		{{.OutputPackageName}}: The name of the package to which the generated files will belong
			Note that this is the real package name, not an alias
			If the "{{.OutputFolderPath}}" flag value points to an existing package, this flag must match it's name (otherwise: generated files won't compile)
			If not set, it will be inferred from the "{{.SourcePackage}}" flag value
			If the "{{.Client}}" flag is set to true, this flag cannot be set
			{{.OutputPackageNameDefault}}
		{{.Client}}: Set to true to generate client code
			Currently, the only supported language is Typescript
			{{.ClientDefault}}

	Other flags:
		{{.Debug}}: This flag controls the level of runtime log information
			If true, additional runtime information will be displayed (aka DEBUG logs)
			{{.DebugDefault}}
		{{.SupportsAnsi}}: If true, the log output will contain ANSI characters
			Set it to true if you terminal supports ANSI characters
			Currently, ANSI escape codes are used to colorize strings and draw "nested logs"
			Some control sequences can only access visible terminal content. Logs with many "nested child logs" will behave unexpectedly
			If you need to debug something, it can help you
			** EXPERIMENTAL FEATURE **
			{{.SupportsAnsiDefault}}
		{{.OnlyAsciiTypeNames}}: If true, the generated typeNames will contain only ASCII characters
			If this flag is omitted, the typeNames will contain some unusual unicode characters to improve readability
			Note that in some systems this flag needs to be true, due to lack of full unicode support
			{{.OnlyAsciiTypeNamesDefault}}
		{{.Workdir}}: Use this flag to set the current work directory
			This will be used to resolve package import paths and other things related to the underlying build tool
			{{.WorkdirDefault}}
`
