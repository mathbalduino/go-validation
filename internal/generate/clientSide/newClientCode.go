package clientSide

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goFile"
)

func newClientCode() *clientCode {
	return &clientCode{
		// This dummyFile is used to avoid name clashes, so, it's safe to ignore the constructor params
		dummyFile:  goFile.NewGoFile("", "", ""),
		builtTypes: map[string]bool{},
	}
}

type clientCode struct {
	dummyFile  *goFile.GoFile
	builtTypes map[string]bool
}
