package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

func SchemaRootedFieldsMethod(fieldName, structTypeIdentifier, loxeValidatorImportAlias string) (string, error) {
	type tmplVars struct {
		SchemaRootedFieldsTypeName           string
		TestSchemaRootedFieldsTypeName       string
		SchemaTypeIdentifier                 string
		StructTypeIdentifier                 string
		LoxeValidatorTypeIdentifier          string
		LoxeValidatorSetFieldErrorMethodName string
		LoxeValidatorFieldMethodName         string
		TestRuleTypeName                     string
		FieldName                            string
		FieldMetaName                        string
		TestSuiteTypeIdentifier              string
	}
	vars := tmplVars{
		schemaRootedFieldsTypeName(structTypeIdentifier),
		testSchemaRootedFieldsTypeName(structTypeIdentifier),
		schemaTypeName(structTypeIdentifier),
		structTypeIdentifier,
		fmt.Sprintf("%s.%s", loxeValidatorImportAlias, loxeValidatorTypeName),
		applyFieldErrorMethodName,
		fieldMethodName,
		testRuleTypeName(structTypeIdentifier),
		fieldName,
		fmt.Sprintf("%s.%s", StructMetaVarName(structTypeIdentifier), fieldName),
		fmt.Sprintf("%s.%s", loxeValidatorImportAlias, testSuiteTypeName),
	}

	var data bytes.Buffer
	e := schemaRootedFieldsMethod.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

// -----

var schemaRootedFieldsMethod = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplSchemaRootedFieldsMethod)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

const schemaRootedFieldsMethodName = "RootedFields"

func schemaRootedFieldsTypeName(structTypeIdentifier string) string {
	return "SchemaRootedFields" + identifierToTypeName("<"+structTypeIdentifier+">")
}
func testSchemaRootedFieldsTypeName(structTypeIdentifier string) string {
	return "Test" + schemaRootedFieldsTypeName(structTypeIdentifier)
}

const tmplSchemaRootedFieldsMethod = `
func (prevFn {{.SchemaRootedFieldsTypeName}}) {{.FieldName}}(rule func({{.StructTypeIdentifier}}) error) {{.SchemaRootedFieldsTypeName}} {
	return func(struct_ {{.StructTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}} {
		v := prevFn(struct_)
		v.{{.LoxeValidatorSetFieldErrorMethodName}}({{.FieldMetaName}}, rule(struct_))
		return v
	}
}

func (prevFn {{.TestSchemaRootedFieldsTypeName}}) {{.FieldName}}(test {{.TestRuleTypeName}}) {{.TestSchemaRootedFieldsTypeName}} {
	return func(s {{.TestSuiteTypeIdentifier}}, schema {{.SchemaTypeIdentifier}}) {
		prevFn(s, schema)
		test(s, func(value {{.StructTypeIdentifier}}) error {
			return schema(value).{{.LoxeValidatorFieldMethodName}}({{.FieldMetaName}})
		})
	}
}
`
