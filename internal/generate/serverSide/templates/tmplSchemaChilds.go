package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

func SchemaChilds(structTypeIdentifier, loxeValidatorImportAlias string) (string, error) {
	type tmplVars struct {
		SchemaTypeName              string
		TestSchemaTypeName          string
		SchemaChildsTypeName        string
		LoxeValidatorTypeIdentifier string
		SchemaChildsMethodName      string
		StructTypeIdentifier        string
		TestSchemaChildsTypeName    string
		TestSuiteTypeIdentifier     string
	}
	vars := tmplVars{
		schemaTypeName(structTypeIdentifier),
		testSchemaTypeName(structTypeIdentifier),
		schemaChildsTypeName(structTypeIdentifier),
		fmt.Sprintf("%s.%s", loxeValidatorImportAlias, loxeValidatorTypeName),
		schemaChildsMethodName,
		structTypeIdentifier,
		testSchemaChildsTypeName(structTypeIdentifier),
		fmt.Sprintf("%s.%s", loxeValidatorImportAlias, testSuiteTypeName),
	}

	var data bytes.Buffer
	e := schemaChilds.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

// -----

var schemaChilds = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplSchemaChilds)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

const schemaChildsMethodName = "Childs"

func schemaChildsTypeName(structTypeIdentifier string) string {
	return "SchemaChilds" + identifierToTypeName("<"+structTypeIdentifier+">")
}
func testSchemaChildsTypeName(structTypeIdentifier string) string {
	return "Test" + schemaChildsTypeName(structTypeIdentifier)
}

const tmplSchemaChilds = `
type {{.SchemaChildsTypeName}} func(struct_ {{.StructTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}}
type {{.TestSchemaChildsTypeName}} func(s {{.TestSuiteTypeIdentifier}}, schema {{.SchemaTypeName}})

func (prevFn {{.SchemaTypeName}}) {{.SchemaChildsMethodName}}(callback func(schema {{.SchemaChildsTypeName}}) {{.SchemaChildsTypeName}}) {{.SchemaTypeName}} {
	return func(struct_ {{.StructTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}} {
		v := prevFn(struct_)
		return callback(func(struct_ {{.StructTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}} {
			return v
		})(struct_)
	}
}

func (prevFn {{.TestSchemaTypeName}}) {{.SchemaChildsMethodName}}(callback func({{.TestSchemaChildsTypeName}}) {{.TestSchemaChildsTypeName}}) {{.TestSchemaTypeName}} {
	return func(s {{.TestSuiteTypeIdentifier}}, schema {{.SchemaTypeName}}) {
		prevFn(s, schema)
		callback(func(s {{.TestSuiteTypeIdentifier}}, schema {{.SchemaTypeName}}) {})(s, schema)
	}
}
`
