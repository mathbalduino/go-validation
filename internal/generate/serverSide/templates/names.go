package templates

const newLoxeValidatorFnName = "NewLoxeValidator"
const loxeValidatorTypeName = "LoxeValidator"
const comparableErrorTypeName = "ComparableError"
const testSuiteTypeName = "TestSuite"

const appendChildMethodName = "AppendChild"
const applyFieldErrorMethodName = "ApplyFieldError"
const applyRootErrorMethodName = "ApplyRootError"
const fieldMethodName = "Field"
const rootErrorMethodName = "RootError"
const comparableErrorIsMethodName = "Is"
const testSuiteFatalfMethodName = "Fatalf"
const testSuiteHelperMethodName = "Helper"

func StructMetaVarName(structTypeIdentifier string) string {
	return "Meta" + identifierToTypeName("<" + structTypeIdentifier + ">")
}
