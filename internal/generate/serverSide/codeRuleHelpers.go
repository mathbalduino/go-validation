package serverSide

import (
	"gitlab.com/loxe-tools/go-validation/internal/generate/serverSide/templates"
)

func (c *serverSideCoder) codeRuleHelpers(typeIdentifier, loxeImportAlias string) (string, error) {
	// Add a prefix just to distinguish between "string" and "Helper<string>"|"Test<string>"
	if c.isTypeBuilt("Helpers" + typeIdentifier) {
		return "", nil
	}
	c.newTypeBuilt("Helpers" + typeIdentifier)

	testRule, e := templates.TestRule(typeIdentifier, loxeImportAlias)
	if e != nil {
		return "", e
	}
	helpersRule, e := templates.Helpers(typeIdentifier)
	if e != nil {
		return "", e
	}

	return testRule + "\n" +
			helpersRule + "\n",
		nil
}
