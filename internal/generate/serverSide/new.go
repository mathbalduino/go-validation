package serverSide

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goFile"
)

func newServerSideCoder(fileName, outputPackageName, outputPackagePath string) *serverSideCoder {
	file := goFile.NewGoFile(fileName, outputPackageName, outputPackagePath)
	file.MergeImports(baseImports(outputPackagePath))

	return &serverSideCoder{
		file:                         file,
		builtTypes:                   map[string]bool{},
	}
}

type serverSideCoder struct {
	file       *goFile.GoFile
	builtTypes map[string]bool
}
