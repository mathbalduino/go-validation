package generate

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-validation/internal"
	"gitlab.com/loxe-tools/go-validation/internal/generate/input"
)

type fileI interface {
	Save(string, string) error
	Name() string
}

func saveFile(file fileI, arguments input.Arguments, log *logCLI.LogCLI) {
	e := file.Save(
		fmt.Sprintf("%s %s", internal.LibraryModulePath, internal.LibraryModuleVersion),
		arguments.OutputFolderPath,
	)
	if e != nil {
		log.Fatal("%+v", e)
	}
	log.Info("File %s saved...", file.Name())
}
