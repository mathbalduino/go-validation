package loxe

func BasicCollector(v *LoxeValidator) *CollectedForm {
	return Collect(v, func(e error) interface{} {
		return e.Error()
	})
}

func iterateRawValidator(rawValidator *LoxeValidator, c Collector) *CollectedForm {
	collected := &CollectedForm{
		nil,
		make(map[string]interface{}),
		make(map[string]*CollectedForm, len(rawValidator.childs)),
	}

	if rawValidator.rootError != nil {
		collected.RootError = c(rawValidator.rootError)
	}
	for fieldName, fieldError := range rawValidator.fields {
		collected.Fields[fieldName] = c(fieldError)
	}
	for fieldName, currRawChildValidator := range rawValidator.childs {
		collected.Childs[fieldName] = iterateRawValidator(currRawChildValidator, c)
	}

	return collected
}
